11-1
(1)オ
(2)ア
(3)エ
(4)ウ
(5)エ
(6)ウ
(7)イ
(8)カ

11-2
import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class Ex11 implements ServletContextAttributeListener {

    	public void attributeAdded(ServletContextAttributeEvent arg) {
    		System.out.println("警告：格納は禁止されています");
    	}

    public void attributeRemoved(ServletContextAttributeEvent arg)  {
    }

    public void attributeReplaced(ServletContextAttributeEvent arg)  {
    }

}
